// var express = require('express');
var mongoose = require('mongoose');
var os = require('os');

var port = process.env.DB_PORT || '27017';
var host = 'database';
// var mongo_pass = process.env.MONGO_PASSWORD || '';
var mongo_user = process.env.MONGO_DB_USER || '';
var mongo_db = process.env.MONGO_DATABASE || '';

const fs = require("fs"),
      util = require("util");
var mongo_pass = fs.readFileSync(util.format('/run/secrets/%s', 'db_password'), "utf8").trim();

// 3--
var url = 'mongodb://' + mongo_user + ':' + mongo_pass + '@' + host + ':' + port + '/' + mongo_db;
console.log("Connected to MongoDB to: " + url);
/**
 * Initialize the connection.
 * @method init
 **/
mongoose.connect(url, {
    useMongoClient: true,
});

// 2.--mongoose.connect('mongodb://database/myapp', { //1.--mongoose.connect('mongodb://localhost/myapp',
// 	useMongoClient: true,
// 	/* other options */
// });

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Connected to MongoDB to: " + url);
    // console.log("Connected to MongoDB to: mongodb://database/myapp");

});

const express = require('express');

// Constants
const PORT = 3000;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send("<h1>I'm "+os.hostname()+"</h1>\n");
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
