#!/bin/bash
# wait-for-mongo.sh

while ! nc -z database 27017; do  
  echo "$(date) - still trying" 
  sleep 1
done

echo "$(date) - connected successfully"
eval "npm start"